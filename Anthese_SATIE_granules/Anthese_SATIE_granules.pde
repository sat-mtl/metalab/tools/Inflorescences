import java.util.Map;

//Necessary for OSC communication with SATIE:
import oscP5.*;
import netP5.*;
OscP5 oscReceiver;
NetAddress oscDestination = new NetAddress("127.0.0.1",18032);

String projectRoot;
String audioBundlesPath;
// Audio bundles: ajouter ou enlever les noms des repertoires
String[] audioBundles = {"Instrumental", "mots et melismes", "rapides", "lents"};

HashMap<String, String[]> audioCollections = new HashMap<String, String[]>();

//Parameters to control via wekinator:
float azimuth = 0.5; // -180 to 180 ; 0
float rate = 1.0; // 0.0 to 1.0
float lpf = 18000.0;
float gain = 0.0;
float begin = 0.0;
float end = 0.0;
float interval = 0.1;
float attack = 0.01;
float release = 0.1;
float sustain = 0.1;
boolean loaded = false;
int waitLoad = 5000;
int time;
String currentSample;
String processName = "myprocess";


void setup() {
  //Initialize OSC communication
  oscReceiver = new OscP5(this,12000); //listen for OSC messages on port 12000 (Wekinator default)
  // oscDestination = new NetAddress("10.10.30.52",18032); //send to SATIE's default port
  
  // initialize the drawing window
  size( 512, 200, P3D );
  projectRoot = getProjectRoot();
  audioBundlesPath = projectRoot+"Sons/";
  buildAudioCollections();
  loadSamples();
  // Changement des fichiers audio
  currentSample = audioCollections.get("rapides")[0];
  // ------------------------------------------------
  makeProcess(processName, currentSample);
  time = millis();
}


void loadSamples(){  
  for(String bundle : audioBundles){
    // println(bundle.getKey());
    String[] files = audioCollections.get(bundle);
    for(String fname : files){
      // println(fname + " : "+ audioBundlesPath+bundle+"/"+fname);
      loadSample(fname, audioBundlesPath+bundle+"/"+fname);
    };
  };
};

/*
  Send OSC message to SATIE to load an audio file
  @param name A unique name to be used as ID 
  @param path Absolute path to the audio file
*/
void loadSample(String name, String path) {
  OscMessage msg = new OscMessage("/satie/loadSample");
  msg.add(name);
  msg.add(path);
  oscReceiver.send(msg, oscDestination);

};

/*
  Build a hash map (dictionary) of audio bundles and lists of associated files
*/
void buildAudioCollections() {
  for (String bundle : audioBundles) {
    String bundlePath = audioBundlesPath+bundle+"/";
    audioCollections.put(bundle, listFileNames(bundlePath));
  };
};

void draw() {
  if((millis() - time > waitLoad) && !loaded){
    startProcess(processName);
    loaded = true;
  }
// erase the window to black
  background( 0 );
}

/*
  Get the path to the project's root. Assumption is made that the root is this project's parent
  @return String The absolute path to the project's root with the trailing slash
*/
String getProjectRoot() {
  String ret;
  String[] dirs = sketchPath().split("/");
  String[] rootPath = new String[dirs.length - 1];
  for(int i = 0; i < dirs.length - 1; i++){
    rootPath[i] = dirs[i];
  };
  ret = join(rootPath, "/");
  return ret+"/";
};

/*
  Get all the files in a directory as an array of Strings
  @param dir Absolute path to a directory (needs to contain a trailing slash)
*/
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

void mouseMoved()
{
   azimuth = map( mouseX, 0, width, -180, 180 );
   rate= map( mouseY, 0, height, -1, 1 );
   // controlSample();
   // currentSample = int(map(mouseX, 0, width, 0, float(instrumental_files.length -1)));
   // changeSample("boo", instrumental_files[currentSample]);
   // controlSample("instr", -20);
}



//This is called automatically when OSC message is received
void oscEvent(OscMessage theOscMessage) {
 if (theOscMessage.checkAddrPattern("/wek/outputs")==true) {
     if(theOscMessage.checkTypetag("fffffffff")) { 
        float receivedAzimuth = constrain(theOscMessage.get(0).floatValue(), 0.0, 1.0); //get this parameter
        azimuth = map(receivedAzimuth, 0.0, 1.0, -180, 180);
        begin = constrain(theOscMessage.get(1).floatValue(), 0.0, 0.99); //get this parameter
        end = constrain(theOscMessage.get(2).floatValue(), 0.00001, 1.0);
        interval = map(constrain(theOscMessage.get(3).floatValue(), 0.02, 1.0), 0.0, 1.0, 0.005, 2.0);
        rate = map(constrain(theOscMessage.get(4).floatValue(), 0.0, 1.0), 0.0, 1.0, -2.0, 2.0);
        attack = map(constrain(theOscMessage.get(5).floatValue(), 0.0, 1.0), 0.0, 1.0, 0.01, 2.0);
        sustain = map(constrain(theOscMessage.get(6).floatValue(), 0.0, 1.0), 0.0, 1.0, 0.01, 2.0);
        release = map(constrain(theOscMessage.get(7).floatValue(), 0.0, 1.0), 0.0, 1.0, 0.01, 2.0);
        gain = map(theOscMessage.get(8).floatValue(), 0.0, 1.0, -90, -10);
        println("Received new params value from Wekinator");  
      } else {
        println("Error: unexpected params type tag received by Processing");
      }
      setProcessParameterFloat(processName, "aziDeg", azimuth);
      setProcessParameterFloat(processName, "begin", begin);
      setProcessParameterFloat(processName, "end", end);
      setProcessParameterFloat(processName, "interval", interval);
      setProcessParameterFloat(processName, "rate", rate);
      setProcessParameterFloat(processName, "attack", attack);
      setProcessParameterFloat(processName, "rel", release);
      setProcessParameterFloat(processName, "sus", sustain);
      setProcessParameterFloat(processName, "gain", gain);
 }
}



void startProcess(String processName){
OscMessage msg = new OscMessage("/satie/process/eval");
  msg.add(processName); 
  msg.add("start");
  oscReceiver.send(msg, oscDestination);  
}

void stopProcess(String processName){
OscMessage msg = new OscMessage("/satie/process/eval");
  msg.add(processName); 
  msg.add("cleanUp");
  oscReceiver.send(msg, oscDestination);  
}

void deleteNode(String nodeName){
OscMessage msg = new OscMessage("/satie/scene/deleteNode");
  msg.add(nodeName); 
  oscReceiver.send(msg, oscDestination);  
}

void makeProcess(String processName, String sample){
  OscMessage msg = new OscMessage("/satie/scene/createProcess");
  msg.add(processName); 
  msg.add("sampleProcess");
  msg.add("sample");
  msg.add(sample);
  oscReceiver.send(msg, oscDestination);
}


void setProcessParameterFloat(String name, String parameter, float value){
  OscMessage msg = new OscMessage("/satie/process/set");
  msg.add(name); 
  msg.add(parameter);
  msg.add(value);
  oscReceiver.send(msg, oscDestination);
};

void setProcessSample(String name, String sampleName){
  OscMessage msg = new OscMessage("/satie/process/set");
  msg.add(name); 
  msg.add("sample");
  msg.add(sampleName);
  oscReceiver.send(msg, oscDestination);
};


void changeSample(String name, String sample_name) {
  instantiateSample(name, sample_name);
}

void controlLPF(String name) {
  OscMessage msg = new OscMessage("/satie/source/set");
  msg.add(name); 
  msg.add("lpHz");
  msg.add(lpf);
  oscReceiver.send(msg, oscDestination);
}

void controlSample(String name, float gain) {
  OscMessage msg = new OscMessage("/satie/source/set");
  msg.add(name); 
  msg.add("aziDeg");
  msg.add(azimuth);
  msg.add("rate");
  msg.add(rate);
  msg.add("gainDB");
  msg.add(gain);
  oscReceiver.send(msg, oscDestination);
}

void loadSample() {
  OscMessage msg = new OscMessage("/satie/loadSample");
  msg.add("african1"); 
  msg.add("/home/pi/Desktop/Sons/Instrumental/moment 1.R.wav");
  oscReceiver.send(msg, oscDestination);
}

void instantiateSample(String name, String filename) {
  OscMessage msg = new OscMessage("/satie/scene/createSource"); // mySource sndBuffer default bufnum mySample gate 1 gainDB -20
  msg.add(name);
  msg.add("sndBuffer");
  msg.add("default");
  msg.add("bufnum");
  msg.add(filename);
  msg.add("gate");
  msg.add(1);
  msg.add("gainDB");
  msg.add(-10);
  oscReceiver.send(msg, oscDestination);
}

void clearScene(){
  OscMessage msg = new OscMessage("/satie/scene/clear");
  oscReceiver.send(msg, oscDestination);
}

void keyPressed() {
  if (key == ESC) {
    clearScene();
  }
}
